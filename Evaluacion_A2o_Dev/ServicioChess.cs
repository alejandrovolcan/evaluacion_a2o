﻿using System;
using System.ServiceModel;
using System.ServiceProcess;

/// <summary>
/// Programando por el Ing. Alejandro Volcán
/// Correo: alejandrovolcan@gmail.com
/// Creado el 17-02-2021
/// Version 1.0.0
/// </summary>

namespace Evaluacion_A2o_Dev
{
    public partial class ServicioChess : ServiceBase
    {
        ServiceHost host;
        readonly Logs logs = new Logs();

        public ServicioChess()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                host = new ServiceHost(typeof(ServicioCheesWCF.Movimientos));
                host.Open();
            }
            catch(Exception ex)
            {
                logs.LogsWindows(ex.Message, "1");
            }
        }

        protected override void OnStop()
        {
            try
            {
                host.Close();
            }
            catch (Exception ex)
            {
                logs.LogsWindows(ex.Message, "1");
            }
        }        

    }
}

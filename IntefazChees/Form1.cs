﻿using IntefazChees.MovimientosServicio;
using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Windows.Forms;

/// <summary>
/// Programando por el Ing. Alejandro Volcán
/// Correo: alejandrovolcan@gmail.com
/// Creado el 17-02-2021
/// Version 1.0.0
/// </summary>

namespace IntefazChees
{
    public partial class Inicio : Form
    {
        #region Clases y variables Generales

        readonly Logs logs = new Logs();
        private int cont = 0;
        private List<int> ElementosX = new List<int>();
        private List<int> ElementosY = new List<int>();
        private MovimientosServicio.MovimientosClient Servicio = new MovimientosClient();
        ServiceController servicio = new ServiceController("MovimientoReina");

        #endregion

        #region Carga de Form

        public Inicio()
        { InitializeComponent(); }

        private void Incio_Load(object sender, EventArgs e)
        {
            try
            {
                if(servicio.Status == ServiceControllerStatus.Running)
                {
                    BttIniciaServicio.Enabled = false;
                }
                else if (servicio.Status == ServiceControllerStatus.Paused || servicio.Status == ServiceControllerStatus.Stopped)
                {
                    servicio.Start();
                    BttIniciaServicio.Enabled = false;                    
                }
                else
                {
                    BttIniciaServicio.Enabled = true;
                    BttDetieneServicio.Enabled = false;
                    lblEstado.Text = "El servicio se encuentra detenido";
                }
            }
            catch(Exception ex)
            {
                logs.LogsWindows(ex.Message, "1");
                MessageBox.Show("Error en el servicio por favor valide con el departamento técnico", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Botones Tab_01 Calculo de Movimientos

        private void BttCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(TxtTablero.Text) || !String.IsNullOrEmpty(TxtReinaPos.Text))
                {
                    string[] nk = TxtTablero.Text.Split(' ');
                    int n = Convert.ToInt32(nk[0]);
                    int k = Convert.ToInt32(nk[1]);

                    string[] r_qC_q = TxtReinaPos.Text.Split(' ');
                    int rq = Convert.ToInt32(r_qC_q[0]);
                    int cq = Convert.ToInt32(r_qC_q[1]);

                    if (n == 0 || rq == 0 || cq == 0)
                    {
                        TxtObst.Text = "";
                        MessageBox.Show("Alguno de los valores no son válidos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (rq > n || cq > n)
                    {
                        MessageBox.Show("La reina se encuentra fuera del tablero", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        int[] ObstPosX;
                        int[] ObstPosY;

                        if (k == 0)
                        {
                            ElementosX.Add(0);
                            ElementosY.Add(0);
                        }
                        else
                        {
                            TxtTablero.Enabled = false;
                            TxtReinaPos.Enabled = false;

                            if (!String.IsNullOrEmpty(TxtObst.Text))
                            {
                                string str = TxtObst.Text;
                                string[] arr = str.Split(new string[] { " " }, StringSplitOptions.None);
                                int valorx = Convert.ToInt32(arr[0]);
                                int valory = Convert.ToInt32(arr[1]);
                                if (valorx > n || valory > n)
                                {
                                    MessageBox.Show("El obstáculo se encuentra fuera del tablero", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                else
                                {
                                    cont++;
                                    ElementosX.Add(valorx);
                                    ElementosY.Add(valory);
                                    TxtObst.Text = "";
                                }
                            }
                            else
                            {
                                MessageBox.Show("El valor del obtáculo no es valido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }

                        if (cont == k)
                        {
                            ObstPosX = ElementosX.ToArray();
                            ObstPosY = ElementosY.ToArray();                          
                            string resultado = Convert.ToString(Servicio.MovimientosPosibles(n, k, rq, cq, ObstPosX, ObstPosY));
                            LblResultado.Visible = true;
                            LblResultado.Text = "Los movimientos disponibles son: " + resultado;
                            ElementosX.Clear();
                            ElementosY.Clear();
                            cont = 0;
                            LimpiarDatos();
                        }
                        else
                        {
                            int restante = k - cont;
                            LblResultado.Visible = true;
                            LblResultado.Text = "Por favor ingrese la siguiente posición del obstáculo, debe de ingresar " + restante.ToString() + " más";
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Por favor ingrese los valores necesarios", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                logs.LogsWindows(ex.Message, "1");                
            }
        }

        private void BttLimpiar_Click(object sender, EventArgs e)
        {
            try
            {
                LimpiarDatos();
                LblResultado.Text = "";
            }
            catch (Exception ex)
            {
                logs.LogsWindows(ex.Message, "1");             
            }

        }

        #endregion

        #region Validaciones de datos

        #region Control de escritura de los textbox

        private void TxtTablero_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            SoloNumerosOEspacios(e);
        }

        private void TxtReinaPos_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            SoloNumerosOEspacios(e);
        }

        private void TxtObst_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            SoloNumerosOEspacios(e);
        }

        #endregion

        //Boton de minimizar a la barra de tareas
        private void Minimizar_Click(object sender, EventArgs e)
        {
            MinimizarAplicacion();
        }

        //Maximiza la aplicación
        private void IconTray_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            IconTray.Visible = false;
            WindowState = FormWindowState.Normal;
        }

        //Controla cuando la aplicación se trae al frente o minimiza
        private void Inicio_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            { MinimizarAplicacion(); }
        }

        //No permite el cierre de la aplicación, la minimiza a la barra de tareas
        private void Inicio_FormClosing(object sender, FormClosingEventArgs e)
        {
            switch (e.CloseReason)
            {
                case CloseReason.UserClosing:
                    e.Cancel = true;
                    MinimizarAplicacion();
                    break;
            }
        }

        //Minimiza a la barra de tareas
        private void MinimizarAplicacion()
        {
            try
            {
                WindowState = FormWindowState.Minimized;
                ShowInTaskbar = false;
                IconTray.Visible = true;
                IconTray.BalloonTipText = "Aplicacion Funcionando ";
                IconTray.BalloonTipTitle = "Alejandro Volcan";
                IconTray.ShowBalloonTip(1000);
            }
            catch (Exception ex)
            {
                logs.ErrorLog(this, ex);
            }
        }

        //Permitir el ingreso solo de números o espacio en blanco
        private void SoloNumerosOEspacios(KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            { e.Handled = false; }
            else
            if (Char.IsControl(e.KeyChar))
            { e.Handled = false; }
            else
            if (Char.IsSeparator(e.KeyChar))
            { e.Handled = false; }
            else
            { e.Handled = true; }
        }

        //Limpia los datos de los textbox
        private void LimpiarDatos()
        {
            TxtTablero.Text = "";
            TxtReinaPos.Text = "";
            TxtTablero.Enabled = true;
            TxtReinaPos.Enabled = true;
        }

        #endregion

        #region Botones Tab_02 Manejo del servicio

        private void BttIniciaServicio_Click(object sender, EventArgs e)
        {
            try
            {
                if (servicio != null || servicio.Status == ServiceControllerStatus.Stopped)
                {
                    servicio.Start();
                    BttIniciaServicio.Enabled = false;
                    BttDetieneServicio.Enabled = true;
                    lblEstado.Text = "El servicio se encuentra funcionando";
                    BttCalcular.Enabled = true;
                    logs.LogsWindows("Se inicio el servicio", "3");
                }
                else
                {
                    MessageBox.Show("Error en el servicio por favor valide con el departamento técnico", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch(Exception ex)
            {
                logs.LogsWindows(ex.Message, "1");
            }
        }

        private void BttDetieneServicio_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult resultado = MessageBox.Show("Seguro desea detener el servicio", "Detiene el Servicio", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if(resultado == DialogResult.Yes)
                {
                    servicio.Stop();
                    BttIniciaServicio.Enabled = true;
                    BttDetieneServicio.Enabled = false;
                    lblEstado.Text = "El servicio se encuentra detenido";
                    BttCalcular.Enabled = false;
                    logs.LogsWindows("Se detuvo el servicio", "3");
                }
                else
                {
                    MessageBox.Show("Cancelado", "Información", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            catch (Exception ex)
            {
                logs.LogsWindows(ex.Message, "1");
            }
        }
        #endregion        
    }
}
﻿namespace IntefazChees
{
    partial class Inicio
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Inicio));
            this.IconTray = new System.Windows.Forms.NotifyIcon(this.components);
            this.Minimizar = new FontAwesome.Sharp.IconButton();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.IndexTab = new System.Windows.Forms.TabControl();
            this.Tab_01 = new System.Windows.Forms.TabPage();
            this.BttLimpiar = new FontAwesome.Sharp.IconButton();
            this.LblResultado = new System.Windows.Forms.Label();
            this.BttCalcular = new FontAwesome.Sharp.IconButton();
            this.TxtObst = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtReinaPos = new System.Windows.Forms.TextBox();
            this.LblPosicion = new System.Windows.Forms.Label();
            this.TxtTablero = new System.Windows.Forms.TextBox();
            this.LblSize = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Tab_02 = new System.Windows.Forms.TabPage();
            this.BttIniciaServicio = new FontAwesome.Sharp.IconButton();
            this.BttDetieneServicio = new FontAwesome.Sharp.IconButton();
            this.lblEstado = new System.Windows.Forms.Label();
            this.iconos = new System.Windows.Forms.ImageList(this.components);
            this.IndexTab.SuspendLayout();
            this.Tab_01.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.Tab_02.SuspendLayout();
            this.SuspendLayout();
            // 
            // IconTray
            // 
            this.IconTray.Icon = ((System.Drawing.Icon)(resources.GetObject("IconTray.Icon")));
            this.IconTray.Text = "Chees";
            this.IconTray.Visible = true;
            this.IconTray.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.IconTray_MouseDoubleClick);
            // 
            // Minimizar
            // 
            this.Minimizar.BackColor = System.Drawing.Color.Transparent;
            this.Minimizar.FlatAppearance.BorderSize = 0;
            this.Minimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Minimizar.IconChar = FontAwesome.Sharp.IconChar.MinusSquare;
            this.Minimizar.IconColor = System.Drawing.Color.White;
            this.Minimizar.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.Minimizar.IconSize = 20;
            this.Minimizar.Location = new System.Drawing.Point(525, 1);
            this.Minimizar.Name = "Minimizar";
            this.Minimizar.Size = new System.Drawing.Size(20, 23);
            this.Minimizar.TabIndex = 0;
            this.Minimizar.UseVisualStyleBackColor = false;
            this.Minimizar.Click += new System.EventHandler(this.Minimizar_Click);
            // 
            // lblMensaje
            // 
            this.lblMensaje.AutoSize = true;
            this.lblMensaje.Location = new System.Drawing.Point(4, 407);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(10, 13);
            this.lblMensaje.TabIndex = 1;
            this.lblMensaje.Text = ".";
            // 
            // IndexTab
            // 
            this.IndexTab.Controls.Add(this.Tab_01);
            this.IndexTab.Controls.Add(this.Tab_02);
            this.IndexTab.ImageList = this.iconos;
            this.IndexTab.Location = new System.Drawing.Point(13, 19);
            this.IndexTab.Name = "IndexTab";
            this.IndexTab.SelectedIndex = 0;
            this.IndexTab.Size = new System.Drawing.Size(531, 240);
            this.IndexTab.TabIndex = 2;
            // 
            // Tab_01
            // 
            this.Tab_01.BackColor = System.Drawing.Color.White;
            this.Tab_01.Controls.Add(this.BttLimpiar);
            this.Tab_01.Controls.Add(this.LblResultado);
            this.Tab_01.Controls.Add(this.BttCalcular);
            this.Tab_01.Controls.Add(this.TxtObst);
            this.Tab_01.Controls.Add(this.label4);
            this.Tab_01.Controls.Add(this.TxtReinaPos);
            this.Tab_01.Controls.Add(this.LblPosicion);
            this.Tab_01.Controls.Add(this.TxtTablero);
            this.Tab_01.Controls.Add(this.LblSize);
            this.Tab_01.Controls.Add(this.label1);
            this.Tab_01.Controls.Add(this.pictureBox2);
            this.Tab_01.ImageIndex = 0;
            this.Tab_01.Location = new System.Drawing.Point(4, 23);
            this.Tab_01.Name = "Tab_01";
            this.Tab_01.Padding = new System.Windows.Forms.Padding(3);
            this.Tab_01.Size = new System.Drawing.Size(523, 213);
            this.Tab_01.TabIndex = 0;
            this.Tab_01.Text = "Movimientos Reina";
            // 
            // BttLimpiar
            // 
            this.BttLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BttLimpiar.IconChar = FontAwesome.Sharp.IconChar.Broom;
            this.BttLimpiar.IconColor = System.Drawing.Color.Black;
            this.BttLimpiar.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.BttLimpiar.IconSize = 20;
            this.BttLimpiar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BttLimpiar.Location = new System.Drawing.Point(156, 115);
            this.BttLimpiar.Name = "BttLimpiar";
            this.BttLimpiar.Size = new System.Drawing.Size(103, 36);
            this.BttLimpiar.TabIndex = 16;
            this.BttLimpiar.Text = "Limpiar Datos";
            this.BttLimpiar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BttLimpiar.UseVisualStyleBackColor = true;
            this.BttLimpiar.Click += new System.EventHandler(this.BttLimpiar_Click);
            // 
            // LblResultado
            // 
            this.LblResultado.AutoSize = true;
            this.LblResultado.Location = new System.Drawing.Point(6, 186);
            this.LblResultado.Name = "LblResultado";
            this.LblResultado.Size = new System.Drawing.Size(35, 13);
            this.LblResultado.TabIndex = 15;
            this.LblResultado.Text = "label5";
            this.LblResultado.Visible = false;
            // 
            // BttCalcular
            // 
            this.BttCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BttCalcular.IconChar = FontAwesome.Sharp.IconChar.Calculator;
            this.BttCalcular.IconColor = System.Drawing.Color.Black;
            this.BttCalcular.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.BttCalcular.IconSize = 20;
            this.BttCalcular.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BttCalcular.Location = new System.Drawing.Point(44, 115);
            this.BttCalcular.Name = "BttCalcular";
            this.BttCalcular.Size = new System.Drawing.Size(99, 36);
            this.BttCalcular.TabIndex = 14;
            this.BttCalcular.Text = "Movimientos";
            this.BttCalcular.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BttCalcular.UseVisualStyleBackColor = true;
            this.BttCalcular.Click += new System.EventHandler(this.BttCalcular_Click);
            // 
            // TxtObst
            // 
            this.TxtObst.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtObst.Location = new System.Drawing.Point(129, 73);
            this.TxtObst.MaxLength = 3;
            this.TxtObst.Name = "TxtObst";
            this.TxtObst.Size = new System.Drawing.Size(38, 20);
            this.TxtObst.TabIndex = 11;
            this.TxtObst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtObst_KeyPress_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Posición de Obstaculo";
            // 
            // TxtReinaPos
            // 
            this.TxtReinaPos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtReinaPos.Location = new System.Drawing.Point(130, 45);
            this.TxtReinaPos.MaxLength = 3;
            this.TxtReinaPos.Name = "TxtReinaPos";
            this.TxtReinaPos.Size = new System.Drawing.Size(37, 20);
            this.TxtReinaPos.TabIndex = 5;
            this.TxtReinaPos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtReinaPos_KeyPress_1);
            // 
            // LblPosicion
            // 
            this.LblPosicion.AutoSize = true;
            this.LblPosicion.Location = new System.Drawing.Point(4, 47);
            this.LblPosicion.Name = "LblPosicion";
            this.LblPosicion.Size = new System.Drawing.Size(104, 13);
            this.LblPosicion.TabIndex = 4;
            this.LblPosicion.Text = "Posición de la Reina";
            // 
            // TxtTablero
            // 
            this.TxtTablero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtTablero.Location = new System.Drawing.Point(236, 17);
            this.TxtTablero.MaxLength = 3;
            this.TxtTablero.Name = "TxtTablero";
            this.TxtTablero.Size = new System.Drawing.Size(37, 20);
            this.TxtTablero.TabIndex = 5;
            this.TxtTablero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtTablero_KeyPress_1);
            // 
            // LblSize
            // 
            this.LblSize.AutoSize = true;
            this.LblSize.Location = new System.Drawing.Point(4, 19);
            this.LblSize.Name = "LblSize";
            this.LblSize.Size = new System.Drawing.Size(226, 13);
            this.LblSize.TabIndex = 2;
            this.LblSize.Text = "Tamaño del Tablero y Cantidad de Obstaculos";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(372, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Imagen Referencial";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(325, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(192, 145);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // Tab_02
            // 
            this.Tab_02.BackColor = System.Drawing.Color.White;
            this.Tab_02.Controls.Add(this.BttIniciaServicio);
            this.Tab_02.Controls.Add(this.BttDetieneServicio);
            this.Tab_02.Controls.Add(this.lblEstado);
            this.Tab_02.ImageKey = "plug.png";
            this.Tab_02.Location = new System.Drawing.Point(4, 23);
            this.Tab_02.Name = "Tab_02";
            this.Tab_02.Size = new System.Drawing.Size(523, 213);
            this.Tab_02.TabIndex = 1;
            this.Tab_02.Text = "Configuración";
            // 
            // BttIniciaServicio
            // 
            this.BttIniciaServicio.IconChar = FontAwesome.Sharp.IconChar.None;
            this.BttIniciaServicio.IconColor = System.Drawing.Color.Black;
            this.BttIniciaServicio.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.BttIniciaServicio.Location = new System.Drawing.Point(85, 139);
            this.BttIniciaServicio.Name = "BttIniciaServicio";
            this.BttIniciaServicio.Size = new System.Drawing.Size(126, 23);
            this.BttIniciaServicio.TabIndex = 2;
            this.BttIniciaServicio.Text = "Iniciar Servicio";
            this.BttIniciaServicio.UseVisualStyleBackColor = true;
            this.BttIniciaServicio.Click += new System.EventHandler(this.BttIniciaServicio_Click);
            // 
            // BttDetieneServicio
            // 
            this.BttDetieneServicio.IconChar = FontAwesome.Sharp.IconChar.None;
            this.BttDetieneServicio.IconColor = System.Drawing.Color.Black;
            this.BttDetieneServicio.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.BttDetieneServicio.Location = new System.Drawing.Point(231, 139);
            this.BttDetieneServicio.Name = "BttDetieneServicio";
            this.BttDetieneServicio.Size = new System.Drawing.Size(126, 23);
            this.BttDetieneServicio.TabIndex = 1;
            this.BttDetieneServicio.Text = "Detener Servicio";
            this.BttDetieneServicio.UseVisualStyleBackColor = true;
            this.BttDetieneServicio.Click += new System.EventHandler(this.BttDetieneServicio_Click);
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.Location = new System.Drawing.Point(82, 71);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(182, 13);
            this.lblEstado.TabIndex = 0;
            this.lblEstado.Text = "El servicio se encuentra funcionando";
            // 
            // iconos
            // 
            this.iconos.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("iconos.ImageStream")));
            this.iconos.TransparentColor = System.Drawing.Color.Transparent;
            this.iconos.Images.SetKeyName(0, "reina-blanca-ilustración.png");
            this.iconos.Images.SetKeyName(1, "images.png");
            this.iconos.Images.SetKeyName(2, "plug.png");
            // 
            // Inicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(553, 271);
            this.Controls.Add(this.IndexTab);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.Minimizar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "Inicio";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Inicio";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Inicio_FormClosing);
            this.Load += new System.EventHandler(this.Incio_Load);
            this.Resize += new System.EventHandler(this.Inicio_Resize);
            this.IndexTab.ResumeLayout(false);
            this.Tab_01.ResumeLayout(false);
            this.Tab_01.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.Tab_02.ResumeLayout(false);
            this.Tab_02.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon IconTray;
        private FontAwesome.Sharp.IconButton Minimizar;
        private System.Windows.Forms.Label lblMensaje;
        private System.Windows.Forms.TabControl IndexTab;
        private System.Windows.Forms.TabPage Tab_01;
        private FontAwesome.Sharp.IconButton BttLimpiar;
        private System.Windows.Forms.Label LblResultado;
        private FontAwesome.Sharp.IconButton BttCalcular;
        private System.Windows.Forms.TextBox TxtObst;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtReinaPos;
        private System.Windows.Forms.Label LblPosicion;
        private System.Windows.Forms.TextBox TxtTablero;
        private System.Windows.Forms.Label LblSize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabPage Tab_02;
        private System.Windows.Forms.ImageList iconos;
        private System.Windows.Forms.Label lblEstado;
        private FontAwesome.Sharp.IconButton BttIniciaServicio;
        private FontAwesome.Sharp.IconButton BttDetieneServicio;
    }
}


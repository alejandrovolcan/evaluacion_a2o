﻿
namespace Evaluacion_A2o_Dev
{
    class eventosLogs
    {

        string origen;
        /// Nombre de la aplicación o servicio que genera el evento        
        public string Origen
        {
            get
            { return origen; }
            set
            { origen = value; }
        }

        string tipoOrigen;
        /// Tipo de evento a anotar. Posibles opciones (Application / System / nombre personalizado)        
        public string TipoOrigen
        {
            get
            { return tipoOrigen; }
            set
            { tipoOrigen = value; }
        }

        string evento;
        /// Nombre del evento a auditar        
        public string Evento
        {
            get
            { return evento; }
            set
            { evento = value; }
        }

        string mensaje;
        /// Texto del mensaje a anotar
        public string Mensaje
        {
            get
            { return mensaje; }
            set
            { mensaje = value; }
        }

        string tipoEntrada;        
        /// Tipo de entrada para el evento.  Posibles opciones (1=Error/2=FailureAudit/3=Information/4=SuccessAudit/5=Warning)        
        public string TipoEntrada
        {
            get
            { return tipoEntrada; }
            set
            { tipoEntrada = value; }
        }


    }
}

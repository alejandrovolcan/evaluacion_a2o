﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;

/// <summary>
/// Programando por el Ing. Alejandro Volcán
/// Correo: alejandrovolcan@gmail.com
/// Creado el 17-02-2021
/// Version 1.0.0
/// </summary>

namespace Evaluacion_A2o_Dev
{
    class Logs
    {
        #region Log Interno

        public void ErrorLog(object Fuente, Exception error)
        {
            string rutaCarpeta = ConfigurationManager.AppSettings.Get("CarpetaLogs");
            string ArchivoLog = rutaCarpeta + @"\MueveReina.txt";
            StackTrace stacktrace = new StackTrace();

            if (Directory.Exists(rutaCarpeta))
            {
                if (File.Exists(ArchivoLog))
                {
                    StreamWriter sw = File.AppendText(ArchivoLog);
                    sw.WriteLine("--> Error Presentado el " + DateTime.Today.ToString("dd-MM-yy ") + DateTime.Now.ToString("HH:mm:ss"));
                    sw.WriteLine(Fuente.GetType().FullName);
                    sw.WriteLine(stacktrace.GetFrame(1).GetMethod().Name + " - " + error);
                    sw.Close();
                }
                else
                {
                    StreamWriter sw = new StreamWriter(ArchivoLog);
                    sw.WriteLine("--> Error Presentado el " + DateTime.Today.ToString("dd-MM-yy ") + DateTime.Now.ToString("HH:mm:ss"));
                    sw.WriteLine(Fuente.GetType().FullName);
                    sw.WriteLine(stacktrace.GetFrame(1).GetMethod().Name + " - " + error);
                    sw.Close();
                }
            }
            else
            {
                Directory.CreateDirectory(rutaCarpeta);
                StreamWriter sw = new StreamWriter(ArchivoLog);
                sw.WriteLine("--> Error Presentado el " + DateTime.Today.ToString("dd-MM-yy ") + DateTime.Now.ToString("HH:mm:ss"));
                sw.WriteLine(Fuente.GetType().FullName);
                sw.WriteLine(stacktrace.GetFrame(1).GetMethod().Name + " - " + error);
                sw.Close(); ;
            }
        }

        #endregion

        #region Metodo para escribir en el log de eventos en Microsoft

        public void LogsWindows(string mensaje, string tipo)
        {
            try
            {
                eventosLogs evento = new eventosLogs
                {
                    Origen = "MueveReina",
                    TipoOrigen = "MueveReina",
                    Evento = "winEventos",
                    Mensaje = mensaje,
                    TipoEntrada = tipo
                };
                //Anota el evento en el visor de sucesos
                EscribirMensajeLog(evento);

            }
            catch (Exception ex)
            { ErrorLog(this, ex); }
        }

        public void EscribirMensajeLog(eventosLogs evento)
        {
            try
            {
                EventLog miLog = new EventLog(evento.TipoOrigen, ".", evento.Origen);
                //Comprobamos si existe el registro de sucesos
                if (!EventLog.SourceExists(evento.Origen))
                {
                    //Si no existe el registro de sucesos, lo creamos
                    EventLog.CreateEventSource(evento.Origen, evento.TipoOrigen);
                }
                else
                {
                    // Recupera el registro de sucesos correspondiente del origen.
                    evento.TipoOrigen = EventLog.LogNameFromSourceName(evento.Origen, ".");
                }

                miLog.Source = evento.Origen;
                miLog.Log = evento.TipoOrigen;

                //Comprobamos el tipo de anotación y grabamos el evento
                switch (evento.TipoEntrada)
                {
                    case "1":
                        miLog.WriteEntry(evento.Mensaje, EventLogEntryType.Error);
                        break;
                    case "2":
                        miLog.WriteEntry(evento.Mensaje, EventLogEntryType.FailureAudit);
                        break;
                    case "3":
                        miLog.WriteEntry(evento.Mensaje, EventLogEntryType.Information);
                        break;
                    case "4":
                        miLog.WriteEntry(evento.Mensaje, EventLogEntryType.SuccessAudit);
                        break;
                    case "5":
                        miLog.WriteEntry(evento.Mensaje, EventLogEntryType.Warning);
                        break;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion
    }
}

﻿using System.ServiceModel;

/// <summary>
/// Programando por el Ing. Alejandro Volcán
/// Correo: alejandrovolcan@gmail.com
/// Creado el 17-02-2021
/// Version 1.0.0
/// </summary>

namespace ServicioCheesWCF
{    
    [ServiceContract]
    public interface IMovimientos
    {
        [OperationContract]
        int MovimientosPosibles(int n, int k, int x, int y, int[] obstPosx, int[] obstPosy);
    }
}
